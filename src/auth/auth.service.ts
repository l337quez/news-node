import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { loginDto } from 'src/article/common/login.dto';
import { PASS_LOGIN } from 'src/config/config';

const logger = new Logger('AUTH-SERVICE');


@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService
  ) {  }

  /**
   * LOGIN
   * Returns the Bearer-Token if credentials are valid's.
   * @param res
   * @param userModel
   */
  async authenticate(res, login: loginDto) {
    try {
      const payload = { email: login.email};

      const response = {
          token :  this.jwtService.sign(payload),
          user: {
              email: login.email,
          }
      };

      return res.status(HttpStatus.OK).send({
        message: 'Successful registration.',
        data: response,
      });
    } catch (error) {
      logger.error(error);
      return res
        .status(HttpStatus.CONFLICT)
        .send({ message: 'No valid Credentials', error: error });
    }
  }


  /**
   * Validate user's credentials
   * @param email
   * @param pass
   */
   async validateUser(email: string, pass: string) {
    const valid = await bcrypt.compare(pass, PASS_LOGIN);
    return valid ;
  }
}
