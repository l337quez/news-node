import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {

    constructor() {
        const JWT_SECRET = '1A5R%rnmTtF0jNHi%Qt5oP9:!KzxP9)@'
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: JWT_SECRET
        });
    }

    async validate(payload: any) {
        return { email: payload.email };
    }
}

// TODO JWT_SECRET  ponerla en en el .env NO PUEDE ESTAR AQUI