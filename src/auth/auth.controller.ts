import { Controller, Post, Res, Request, Body, UseGuards } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { loginDto } from 'src/article/common/login.dto';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';

@Controller('api/auth/')
export class AuthController {
    constructor(private authService: AuthService) {}


    /**
     * User's login
     * @param res
     * @param req
     */
     @UseGuards(LocalAuthGuard)
     @Post('login')
     @ApiOperation({summary: 'Authentication and token generation.'})
     @ApiResponse({
       status: 200,
       description: 'Successful registration'
     })
     @ApiResponse({
       status: 500,
       description: 'Internal server error'
     })
     @ApiResponse({
      status: 401,
      description: 'Invalid credentials'
    })
    @ApiBody({
      schema:{
        type: 'object',
        properties:{
          email:{
            type: 'string',
            example: "email@email.com"
          },
          password:{
            type: 'string',
            example: '123'
          }
        }
      }
    })
     async login(@Res() res, @Request() req, @Body()  login: loginDto) {
         return this.authService.authenticate(res, login);
     }
 

}
