import { Controller, Get, Post, Body, Patch, Param, Delete, Query, Res, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiParam, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { ArticleService } from './article.service';
import { PaginationQueryDto } from './common/pagination.dto';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';


@ApiTags('Articles Module')
@Controller('api/article/')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}


  @Get()
  @ApiOperation({summary: 'Get all data, paging Optional.'})
  @ApiResponse({
    status: 200,
    description: 'All data list'
  })
  @ApiResponse({
    status: 500,
    description: 'Internal server error'
  })
  @ApiQuery({
    name: 'limit',
    type: 'integer',
    required: false,
  })
  @ApiQuery({
    name: 'page',
    type: 'integer',
    required: false,
  })
  async findAll(@Res() res, @Query() pagination: PaginationQueryDto, ) {
    return await this.articleService.findAll(res, pagination);
  }



  @Get('filter')
  @ApiOperation({summary: 'Get all the records that match the search parameter. Paginated with a limit of 5 records, then filter by author, _tags, title and month.'})
  @ApiResponse({
    status: 200,
    description: 'All data list'
  })
  @ApiResponse({
    status: 500,
    description: 'Internal server error'
  })
  @ApiQuery({
    name: 'limit',
    type: 'integer',
    required: false,
  })
  @ApiQuery({
    name: 'page',
    type: 'integer',
    required: false,
  })
  @ApiQuery({
    name: 'param',
    type: 'string',
    required: true,
  })
  async search(@Res() res, @Query() pagination: PaginationQueryDto, @Query('param') param: string, @Query('login') login: string) {
    return await this.articleService.findByFilter(res,pagination, param);
  }



  @Delete()
  @ApiOperation({summary: 'Delete record.'})
  @ApiParam({
    name: 'id',
    type: 'integer',
    description: 'Enter unique id',
    required: true
  })
  @ApiResponse({
    status: 200,
    description: 'Delete record'
  })
  remove(@Res() res,@Param('id') id: string) {
    return this.articleService.remove(res, id);
  }
}
