import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository, ObjectID as ObjectIDType, Repository } from 'typeorm';
import { ObjectID } from 'mongodb';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import { Article } from './entities/article.entity';
import { HighlightResult } from 'src/highlight-result/entities/highlight-result.entity';
import { HighlightResultService } from 'src/highlight-result/highlight-result.service';
import { PaginationQueryDto } from './common/pagination.dto';

const logger = new Logger('ARTICLE-SERVICE');

@Injectable()
export class ArticleService {
  constructor(
    @InjectRepository(Article)
    private readonly articleRepository: MongoRepository<Article>,
    private highlightResultService: HighlightResultService,
  ) {}

  async create(createArticleDto: CreateArticleDto) {
    const article = new Article();
    return await this.articleRepository.insertOne(article);
  }


  /**
   * Create Article Using Cron Job
   * @param res
   * @param userModel
   */
  async createByCronJob(json: any) {
    try {
      const data = json.data.hits[0];

      // Compare the current objectID with the last objectID saved in the database
      const last_saved_article = await this.articleRepository.findOne({
        where: { objectID: data.ObjectID },
      });


      if (!last_saved_article){

      // save high light result
      const high_light_result = new HighlightResult();
      high_light_result.value_title = data._highlightResult.title?.value;
      high_light_result.value_title_match_level = data._highlightResult.title?.matchLevel
      high_light_result.value_title_match_words = data._highlightResult.title?.matchedWords;
      high_light_result.value_title_fullyHighlighted = data._highlightResult.title?.fullyHighlighted ;

      high_light_result.value_comment_text = data._highlightResult.comment_text?.value;
      high_light_result.value_comment_text_match_level = data._highlightResult.comment_text?.matchLevel
      high_light_result.value_comment_text_match_words = data._highlightResult.comment_text?.matchedWords;
      high_light_result.value_comment_text_fullyHighlighted = data._highlightResult.comment_text?.fullyHighlighted ;

      high_light_result.value_author = data._highlightResult.author?.value;
      high_light_result.value_author_match_level = data._highlightResult?.author.matchLevel
      high_light_result.value_author_match_words = data._highlightResult.author?.matchedWords;
      high_light_result.value_author_fullyHighlighted = data._highlightResult.author?.fullyHighlighted ;


      high_light_result.value_story_text = data._highlightResult.story_text?.value;
      high_light_result.value_story_text_match_level = data._highlightResult.story_text?.matchLevel;
      high_light_result.value_story_text_match_words = data._highlightResult.story_text?.matchedWords;
      high_light_result.value_story_text_fullyHighlighted = data._highlightResult.story_text?.fullyHighlighted ;

      high_light_result.value_story_title = data._highlightResult.story_title?.value;
      high_light_result.value_story_title_match_level = data._highlightResult.story_title?.matchLevel;
      high_light_result.value_story_title_match_words = data._highlightResult.story_title?.matchedWords;
      high_light_result.value_story_title_fullyHighlighted = data._highlightResult.story_title?.fullyHighlighted ;

      high_light_result.value_story_url = data._highlightResult.story_url?.value;
      high_light_result.value_story_url_match_level = data._highlightResult.story_url?.matchLevel;
      high_light_result.value_story_url_match_words = data._highlightResult.story_url?.matchedWords;
      high_light_result.value_story_url_fullyHighlighted = data._highlightResult.story_url?.fullyHighlighted ;

      const highlightResult =  await this.highlightResultService.createByCronJob(high_light_result);

      // save article
      const article = new Article();
      article.createdAt = new Date(data.created_at);
      article.title = data.title;
      article.url = data.url;
      article.author = data.author;
      article.points = data.points;
      article.story_text = data.story_text;
      article.comment_text = data.comment_text;
      article.num_comments = data.num_comments;
      article.story_id = data.story_id;
      article.story_title = data.story_title;
      article.story_url = data.story_url;
      article.parent_id = data.parent_id;
      article.created_at_i = new Date(data.created_at_i * 1000);
      article._tags = data._tags;
      article.objectID = data.objectID;
      article.high_light_result_id = highlightResult.insertedId.toString()

      return await this.articleRepository.insertOne(article);
       }
    } catch (error) {
      logger.error(error);
    }
  }


  // Find All Articles
  async findAll(res, { limit, page }: PaginationQueryDto): Promise<Article[]> {
    try {

      const articles = await this.articleRepository.find({ 
      skip: page,
      take: limit });
      res.status(HttpStatus.OK).send({
        message: 'These are the Articles Found.',
        data: articles
    });
    } catch (error) {
      logger.error(error);
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({
          message: 'An error has occurred and the list of articles cannot show.',
          error: error
      });
    }
  }







  // Find All and Filters
  async findByFilter(res, { limit, page }: PaginationQueryDto, param : string): Promise<Article[]> {
    limit = limit > 5 ? 5 : limit
    try{
      const month = new Date(`${param} 18, 2022`)

      let options : {} ={
        skip: page,
        take: limit,
        where: {
          $or:[
            {author: { $regex :`${param}`}},
            {title: { $regex :`${param}`}},
            {_tags: { $regex :`${param}`}},
            {$expr:{ $and: [{  $eq: [
              {
                $month: "$createdAt"
              },
              {
                $month: month
              }
            ]}]}}
          ]}
        }
        
        const response =  await this.articleRepository.find(options);
        
      res.status(HttpStatus.OK).send({
        message: 'These are the Articles Found.',
        total: response.length,
        data: response
    });
    } catch (error) {
      logger.error(error);
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({
          message: 'An error has occurred and the list of articles cannot show.',
          error: error
      });
    }
  }




  // Delete
  async remove(res, id: string) {
    try{
       const response =  await this.articleRepository.findOneAndDelete({ _id: new ObjectID(id) });
        
       res.status(HttpStatus.OK).send({
         message: 'Record deleted successfully.',
         data: response})

    } catch (error) {
      logger.error(error);
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send({
          message: 'Failed to delete record.',
          error: error
      });
    }
  }


}
