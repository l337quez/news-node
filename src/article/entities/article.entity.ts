import {  Column, Entity, ObjectID, ObjectIdColumn } from "typeorm";

    
@Entity()
export class Article  {
    
    @ObjectIdColumn()
    _id: ObjectID
    
    @Column({type: 'timestamp'})
    createdAt: Date;

    @Column()
    title: string;

    @Column()
    url: string;

    @Column()
    author: string;

    @Column()
    points: string;

    @Column()
    story_text: string;

    @Column()
    comment_text: string;

    @Column()
    num_comments: number;

    @Column()
    story_id: string;
    
    @Column()
    story_title: string;

    @Column()
    story_url: string;

    @Column()
    parent_id: string;
    
    @Column({type: 'timestamp'})
    created_at_i: Date;

    @Column()
    _tags: Array<String>;

    @Column()
    objectID: string;

    @Column()
    high_light_result_id: string
}
