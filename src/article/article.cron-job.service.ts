import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository, Repository } from 'typeorm';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import { Article } from './entities/article.entity';
import { lastValueFrom, map } from 'rxjs';
import { HttpService } from '@nestjs/axios';
import { ArticleService } from './article.service';

const logger = new Logger('ARTICLE-CRON-JOB');

@Injectable()
export class ArticleCronJob {
  constructor(
  private readonly httpService: HttpService,
  private readonly articleService: ArticleService
  ){
  }


  @Cron('* * 1 * * *')
  async handleCron() {
    try {
      const response= await lastValueFrom(this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs'))
     const res= await this.articleService.createByCronJob(response)
      if (res)  logger.verbose("Data saved Successfully")
      
    } catch (error) {
    logger.error(error);
    throw new Error(error);
    }
  }
}



