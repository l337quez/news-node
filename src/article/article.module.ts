import { Module } from '@nestjs/common';
import { ArticleService } from './article.service';
import { ArticleController } from './article.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Article } from './entities/article.entity';
import { HttpModule, HttpService } from '@nestjs/axios';
import { ArticleCronJob } from './article.cron-job.service';
import { HighlightResultService } from 'src/highlight-result/highlight-result.service';
import { HighlightResult } from 'src/highlight-result/entities/highlight-result.entity';
import { LocalStrategy } from 'src/auth/strategies/local.strategy';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JWT_EXPIRATION_TIME, JWT_SECRET } from 'src/config/config';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Article,
    HighlightResult]),
    HttpModule,
  ],
  controllers: [ArticleController],
  providers: [
    ArticleService,
    ArticleCronJob,
    HighlightResultService
  ]
})
export class ArticleModule {}
