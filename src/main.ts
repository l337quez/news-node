import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions:{
        enableImplicitConversion: true
      }
    })
  )

  
 // Config Swagger
  const options_swagger = new DocumentBuilder()
  .setTitle('News Node JS')
  .setVersion('1.0')
  // .addBearerAuth()
  .build();
 
  const document = SwaggerModule.createDocument(app, options_swagger);
  SwaggerModule.setup('api/docs', app, document, {
    explorer: true,
    swaggerOptions:{
      filter: true,
      showRequestDuration: true,
    }
  })

  // Run app
  await app.listen(3000);
}
bootstrap();
