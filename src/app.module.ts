import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticleModule } from './article/article.module';
import { ArticleService } from './article/article.service';
import { Article } from './article/entities/article.entity';
import { AuthModule } from './auth/auth.module';
import { LocalStrategy } from './auth/strategies/local.strategy';
import { JWT_EXPIRATION_TIME, JWT_SECRET } from './config/config';
import { HighlightResultModule } from './highlight-result/highlight-result.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      host: 'localhost',
      url: 'mongodb://localhost/db_test',
      entities: [
        Article
    ],
    synchronize: true,
    useUnifiedTopology: true,
    autoLoadEntities: true,
    useNewUrlParser: true,
  }),
  ScheduleModule.forRoot(),
  ArticleModule,
  HighlightResultModule,
  AuthModule
],
controllers: [],
providers: [],
})
export class AppModule {}
