import { Test, TestingModule } from '@nestjs/testing';
import { HighlightResultController } from './highlight-result.controller';
import { HighlightResultService } from './highlight-result.service';

describe('HighlightResultController', () => {
  let controller: HighlightResultController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HighlightResultController],
      providers: [HighlightResultService],
    }).compile();

    controller = module.get<HighlightResultController>(HighlightResultController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
