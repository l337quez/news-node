import { Module } from '@nestjs/common';
import { HighlightResultService } from './highlight-result.service';
import { HighlightResultController } from './highlight-result.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HighlightResult } from './entities/highlight-result.entity';

@Module({
  imports:[
    TypeOrmModule.forFeature([HighlightResult]),
  ],
  controllers: [HighlightResultController],
  providers: [HighlightResultService]
})
export class HighlightResultModule {}
