import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { CreateHighlightResultDto } from './dto/create-highlight-result.dto';
import { UpdateHighlightResultDto } from './dto/update-highlight-result.dto';
import { HighlightResult } from './entities/highlight-result.entity';

const logger = new Logger('HIGH-LIGHT-RESULT-SERVICE');

@Injectable()
export class HighlightResultService {
  constructor(@InjectRepository(HighlightResult)
  private readonly highlightResulRepository : MongoRepository<HighlightResult>,
  ){  }

  /**
   * Create HighlightResult Using Cron Job
   * @param res
   * @param userModel
   */
   async createByCronJob(highlightResult: HighlightResult){
    try{
           return await this.highlightResulRepository.insertOne(highlightResult)

    }catch (error) {
      logger.error(error);
    }
  }


  create(createHighlightResultDto: CreateHighlightResultDto) {
    return 'This action adds a new highlightResult';
  }

  findAll() {
    return `This action returns all highlightResult`;
  }

  findOne(id: number) {
    return `This action returns a #${id} highlightResult`;
  }

  update(id: number, updateHighlightResultDto: UpdateHighlightResultDto) {
    return `This action updates a #${id} highlightResult`;
  }

  remove(id: number) {
    return `This action removes a #${id} highlightResult`;
  }
}
