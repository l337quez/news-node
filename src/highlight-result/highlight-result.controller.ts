import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { HighlightResultService } from './highlight-result.service';
import { CreateHighlightResultDto } from './dto/create-highlight-result.dto';
import { UpdateHighlightResultDto } from './dto/update-highlight-result.dto';

@Controller('highlight-result')
export class HighlightResultController {
  constructor(private readonly highlightResultService: HighlightResultService) {}


}
