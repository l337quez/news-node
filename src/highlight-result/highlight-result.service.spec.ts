import { Test, TestingModule } from '@nestjs/testing';
import { HighlightResultService } from './highlight-result.service';

describe('HighlightResultService', () => {
  let service: HighlightResultService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HighlightResultService],
    }).compile();

    service = module.get<HighlightResultService>(HighlightResultService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
