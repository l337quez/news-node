import {  Column, Entity, ObjectID, ObjectIdColumn } from "typeorm";

    
@Entity()
export class HighlightResult  {
    
    @ObjectIdColumn()
    _id: ObjectID
    
    @Column({nullable: true})
    value_title: string ;

    @Column({nullable: true})
    value_title_match_level: string;

    @Column({nullable: true})
    value_title_match_words: Array<string>;

    @Column({nullable: true})
    value_title_fullyHighlighted: boolean


    @Column({nullable: true})
    value_author: string;

    @Column({nullable: true})
    value_author_match_level: string;

    @Column({nullable: true})
    value_author_match_words: Array<string>;

    @Column({nullable: true})
    value_author_fullyHighlighted: boolean


    @Column({nullable: true})
    value_comment_text: string;

    @Column({nullable: true})
    value_comment_text_match_level: string;

    @Column({nullable: true})
    value_comment_text_fullyHighlighted: boolean

    @Column({nullable: true})
    value_comment_text_match_words: Array<string>;


    @Column({nullable: true})
    value_story_title: string;

    @Column({nullable: true})
    value_story_title_match_level: string;

    @Column({nullable: true})
    value_story_title_fullyHighlighted: boolean

    @Column({nullable: true})
    value_story_title_match_words: Array<string>;


    @Column({nullable: true})
    value_story_url: string;

    @Column({nullable: true})
    value_story_url_match_level: string;

    @Column({nullable: true})
    value_story_url_fullyHighlighted: boolean

    @Column({nullable: true})
    value_story_url_match_words: Array<string>;


    @Column({nullable: true})
    value_story_text: string;

    @Column({nullable: true})
    value_story_text_match_level: string;

    @Column({nullable: true})
    value_story_text_fullyHighlighted: boolean

    @Column({nullable: true})
    value_story_text_match_words: Array<string>;

}
