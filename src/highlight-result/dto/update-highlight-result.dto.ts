import { PartialType } from '@nestjs/mapped-types';
import { CreateHighlightResultDto } from './create-highlight-result.dto';

export class UpdateHighlightResultDto extends PartialType(CreateHighlightResultDto) {}
